"""
July 9, 2018

Exercise at implementing object-oriented code for Poisson equation with Dirchlet boundary conditions
-k*laplacian(u) = f
K = thermal diffusivity
by Kentrell Owens
"""

from fenics import *
import matplotlib.pyplot as plt


# Define PoissonSolver class
class PoissonSolver(object):
    
    # Initiliaze the class
    def __init__(self, mesh, RHS, K):
        """
        RHS [string]: must be in C++ syntax
        K [double]
        """
        
        self.mesh = mesh
        self.RHS = RHS
        self.K = K
        self.solved = False

    def set_formulation_and_solve(self, formulation_type = "primal", degree = [1]):
        
        """
        Formulation type [string]: primal or mixed
        degree [list]: len(degree) == 1 (primal) or 2 (mixed)

        Returns the solution, u
        """
        if formulation_type == "primal":
            # define function space
            V = FunctionSpace(self.mesh, "Lagrange", degree[0])

            # define boundary conditions
            u0 = Constant(0.0)
            bc = DirichletBC(V, u0, "on_boundary")

            # define variational problem
            u = TrialFunction(V)
            v = TestFunction(V)
            f = Expression(self.RHS, degree = (degree[0]+1))
            a = self.K * inner(grad(u),grad(v))*dx
            L = f*v*dx

            # compute solution
            u = Function(V)
            solve(a == L, u, bc)
            self.solved = True

            return u


        elif formulation_type == "mixed":
            # Add code for mixed formulation
            pass

        else:
            raise Exception("Invalid entry for formulation type. Please enter 'primal' or mixed.")

    def serialize_field(self, name, field):

        """
        Save the solution to a file in vtk format
        """        
        if self.solved:
            file = File("Poisson/"+ name + ".pvd")
            file << field

        else:
            raise Exception("The problem has not yet been solved. Run 'set_formulation_and_solve' function and then try again.")

    def plot_field(self, field):

        if self.solved:

            plt.figure()
            plot(field)
            plt.show()

        else:
            raise Exception("The problem has not yet been solved. Run 'set_formulation_and_solve' function and then try again.")

# set thermal diffusitivity K
K = 1 # chosen arbitrarily

# Create mesh and define function space
mesh = UnitSquareMesh(32, 32)

# set degree as list for each element
degree = [1]

# define f in C++ syntax as a string
f = "10*exp(-(pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2))/0.02)"

# set formulation type
formulation_type = "primal"


# instantiate Poisson solver class
solver = PoissonSolver(mesh, f, K) 
u = solver.set_formulation_and_solve(formulation_type, degree) # set the variational formulation and solve
solver.serialize_field("time-independent", u) # saved the results to a file
solver.plot_field(u) # plot the results

